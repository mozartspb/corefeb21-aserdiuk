package com.epam.learn.l1;


import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;

import java.util.Locale;

class Main {

    /*
     * Complete the 'findDay' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts following parameters:
     *  1. INTEGER month
     *  2. INTEGER day
     *  3. INTEGER year
     */
    public static void main(String[] args) {

        double a = 12324.134;
        Locale locFR = new Locale("fr");
        Locale us = new Locale("us");
        Locale China = new Locale("ch");
        Locale india = new Locale("fr");

        //add ssh key
//new version idea
//gui
//test for gitlab
        //test from idea

        NumberFormat numberFormat2 = NumberFormat.getInstance(us);
        numberFormat2.setMaximumFractionDigits(2);
        System.out.println("US: $"+numberFormat2.format(a));

        NumberFormat numberFormat24= NumberFormat.getInstance(india);
        System.out.println("India: Rs." + numberFormat24.format(a));

        NumberFormat numberFormat3 = NumberFormat.getInstance(China);
        System.out.println("China: ￥" + numberFormat3.format(a));



        NumberFormat numberFormat1 = NumberFormat.getInstance(locFR);
        System.out.println("France: " + numberFormat1.format(a)+" €");

    }


}






